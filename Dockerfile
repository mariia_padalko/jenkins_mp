FROM python:3.6-jessie

WORKDIR /opt
ADD . /opt

RUN pip install --no-cache-dir requests

ENTRYPOINT python -u /opt/main.py
